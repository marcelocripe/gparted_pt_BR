Repositório oficial do programa "GParted"


https://gitlab.gnome.org/GNOME/gparted/
e
https://gitlab.gnome.org/GNOME/gparted/-/blob/master/po/pt_BR.po?ref_type=heads


Traduções revisadas por marcelocripe:

https://gitlab.com/marcelocripe/gparted_pt_BR

https://gitlab.com/marcelocripe/gparted_pt_BR/-/blob/main/gparted_marcelocripe_pt_BR_05-10-2023.po?ref_type=heads

https://gitlab.com/marcelocripe/gparted_pt_BR/-/blob/main/gparted.desktop


Para utilizar o arquivo "gparted_marcelocripe_pt_BR_05-10-2023.po" e o "lxappearance.desktop", inicie o Emulador de Terminal na pasta onde estão os arquivos que foram baixados e aplique os comandos.


"gparted_marcelocripe_pt_BR_05-10-2023.po":

Comando para converter o arquivo editável da tradução com a extensão ".po" para ".mo".

$ msgfmt gparted_marcelocripe_pt_BR_05-10-2023.po -o gparted.mo


Comando para renomear o arquivo antigo da tradução com a extensão ".mo" que está na pasta do idioma "pt_BR".

$ sudo mv /usr/share/locale/pt_BR/LC_MESSAGES/gparted.mo /usr/share/locale/pt_BR/LC_MESSAGES/gparted_antigo.mo


Comando para copiar o arquivo da tradução com a extensão ".mo" para a pasta do idioma "pt_BR".

$ sudo cp gparted.mo /usr/share/locale/pt_BR/LC_MESSAGES


"gparted.desktop":

Comando para copiar o arquivo com a extensão ".desktop" para a pasta /usr/share/applications.

$ sudo cp gparted.desktop /usr/share/applications

Comando para escrever globalmente todas as entradas dos menus do antiX:

$ sudo desktop-menu --write-out-global
